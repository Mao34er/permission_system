package com.jjmf.permission_system.mapper;

import com.jjmf.permission_system.pojo.Authority;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AuthorityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Authority record);

    int insertSelective(Authority record);

    Authority selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Authority record);

    int updateByPrimaryKey(Authority record);

    List<Authority> findAuthoritiesByRoleId(Integer roleId);

    List<Authority> findAuthorities();

}