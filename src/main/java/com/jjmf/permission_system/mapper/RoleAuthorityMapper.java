package com.jjmf.permission_system.mapper;

import com.jjmf.permission_system.pojo.RoleAuthorityKey;

public interface RoleAuthorityMapper {
    int deleteByPrimaryKey(RoleAuthorityKey key);

    int insert(RoleAuthorityKey record);

    int insertSelective(RoleAuthorityKey record);
}