package com.jjmf.permission_system.pojo;

import java.io.Serializable;

public class Authority implements Serializable {

    private static final long serialVersionUID = -1232904342598073167L;

    private Integer id;

    private String authorityName;

    private String icon;

    private String uri;

    private String permission;

    public Authority(Integer id, String authorityName, String icon, String uri, String permission) {
        this.id = id;
        this.authorityName = authorityName;
        this.icon = icon;
        this.uri = uri;
        this.permission = permission;
    }

    public Authority() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName == null ? null : authorityName.trim();
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri == null ? null : uri.trim();
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission == null ? null : permission.trim();
    }
}