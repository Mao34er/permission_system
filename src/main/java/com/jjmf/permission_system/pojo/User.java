package com.jjmf.permission_system.pojo;

import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = -5118874765465790592L;

    private Integer id;

    private String username;

    private String password;

    private Integer roleId;

    public User(Integer id, String username, String password, Integer roleId) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.roleId = roleId;
    }

    public User() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}