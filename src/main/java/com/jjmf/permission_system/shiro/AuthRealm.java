package com.jjmf.permission_system.shiro;

import com.jjmf.permission_system.mapper.AuthorityMapper;
import com.jjmf.permission_system.mapper.RoleMapper;
import com.jjmf.permission_system.mapper.UserMapper;
import com.jjmf.permission_system.pojo.Authority;
import com.jjmf.permission_system.pojo.Role;
import com.jjmf.permission_system.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * <p>Company:河南巨匠魔方科技有限公司</p>
 *
 * @Description: java类作用描述
 * @Author: 毛小闯
 * @CreateDate: 2018/9/17 16:11
 */
@Service
@Slf4j
public class AuthRealm extends AuthorizingRealm{

    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private AuthorityMapper authorityMapper;
    @Autowired
    private UserMapper userMapper;

    /**
     * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用,负责在应用程序中决定用户的访问控制的方法
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //获取当前登录用户
        User user= (User) principalCollection.getPrimaryPrincipal();
        //授权
        log.debug("授予角色和权限");
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        Integer roleId = user.getRoleId();
        Role role = roleMapper.selectByPrimaryKey(roleId);
        info.addRole(role.getRoleName());
        List<Authority> authorities =  authorityMapper.findAuthoritiesByRoleId(roleId);
        if (authorities.size() == 0){
            return null;
        }
        return info;
    }

    /**
     * 认证回调函数，登录信息和用户验证信息验证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        // TODO: 2018/9/17
        UsernamePasswordToken token= (UsernamePasswordToken) authenticationToken;
        String username = token.getUsername();
        //根据用户名查询密码，由安全管理器负责对比查询出的数据库中的密码和页面输入的密码是否一致
        User user = userMapper.findUserByUserName(username);
        if (user == null){
            return null;
        }
        //单用户登录
        //处理session
//        DefaultWebSecurityManager securityManager= (DefaultWebSecurityManager) SecurityUtils.getSecurityManager();
//        DefaultWebSessionManager sessionManager = (DefaultWebSessionManager) securityManager.getSessionManager();
        //获取当前登录用户的session列表
//        Collection<Session> sessions = sessionManager.getSessionDAO().getActiveSessions();
//        User temp;
//        for (Session session:sessions
//             ) {
            //清除该用户以前登录是保存的session列表,强制退出
//            Object attribute = session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
//            if (attribute==null){
//                continue;
//            }
//            temp = (User) ((SimplePrincipalCollection) attribute).getPrimaryPrincipal();
//            if (username.equals(temp.getUsername())){
//                sessionManager.getSessionDAO().delete(session);
//            }
//
//        }
//        String password = user.getPassword();
        else {
            // 密码存在
            // 第一个参数 ，登陆后，需要在session保存数据
            // 第二个参数，查询到密码(加密规则要和自定义的HashedCredentialsMatcher中的HashAlgorithmName散列算法一致)
            // 第三个参数 ，realm名字DigestUtils.md5Hex(user.getPassword())
            return new SimpleAuthenticationInfo(user,user.getPassword(),
                    getName());
        }
    }
}
