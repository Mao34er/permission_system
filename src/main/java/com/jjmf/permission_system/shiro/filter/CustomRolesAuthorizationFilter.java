package com.jjmf.permission_system.shiro.filter;

import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.RolesAuthorizationFilter;
import org.springframework.http.HttpMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>Company:河南巨匠魔方科技有限公司</p>
 *
 * @Description: 自定义的角色过滤器
 * @Author: 毛小闯
 * @CreateDate: 2018/9/20 10:37
 */
public class CustomRolesAuthorizationFilter extends RolesAuthorizationFilter {
    /**
     *
     * @param request
     * @param response
     * @param mappedValue
     * @return
     * @throws IOException
     */
    @Override
    public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws IOException {
        Subject subject = getSubject(request, response);
        String[] rolesArray = (String[]) mappedValue;
        //如果角色限制为null,直接放行
        if (rolesArray==null||rolesArray.length==0){
            return true;
        }
        for (int i = 0; i < rolesArray.length ; i++) {
            //若当前用户是rolesArrays中的任何一个,则有权限访问
            if (subject.hasRole(rolesArray[i])){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;
        //处理跨域问题,跨域的请求首先会发一个options类型的请求
        if (servletRequest.getMethod().equals(HttpMethod.OPTIONS.name())){
            return true;
        }
        boolean isAccess = isAccessAllowed(request, response, mappedValue);
        if (isAccess){
            return true;
        }
        //设置字符集
        servletResponse.setCharacterEncoding("UTF-8");
        //获取subject
        Subject subject = getSubject(request, response);
        //创建字符打印输出流
        PrintWriter printWriter = servletResponse.getWriter();
        servletResponse.setContentType("application/json;charset=UTF-8");
        servletResponse.setHeader("Access-Control-Allow-Origin", servletRequest.getHeader("Origin"));
        servletResponse.setHeader("Access-Control-Allow-Credentials", "true");
        servletResponse.setHeader("Vary", "Origin");
        String respStr;
        //暂时使用map
        Map<Integer,String> responseMap = new HashMap<>();
        if (subject.getPrincipal()==null){
//            respStr = JSONObject.toJSONString(new BaseResponse<>(300, "您还未登录，请先登录"));
            respStr = JSONObject.toJSONString(responseMap.put(300, "您还未登录，请先登录"));
        } else {
//            respStr = JSONObject.toJSONString(new BaseResponse<>(403, "您没有此权限，请联系管理员"));
            respStr = JSONObject.toJSONString(responseMap.put(403, "您没有此权限，请联系管理员"));
        }
        printWriter.write(respStr);
        printWriter.flush();
        servletResponse.setHeader("content-Length",respStr.getBytes().length+"");
        return  false;
    }
}
