package com.jjmf.permission_system;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.jjmf.permission_system.mapper")
public class PermissionSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(PermissionSystemApplication.class, args);
    }
}
