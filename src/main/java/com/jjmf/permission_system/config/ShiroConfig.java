package com.jjmf.permission_system.config;

import com.jjmf.permission_system.service.ShiroService;
import com.jjmf.permission_system.shiro.AuthRealm;
import com.jjmf.permission_system.shiro.filter.CustomRolesAuthorizationFilter;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>Company:河南巨匠魔方科技有限公司</p>
 *
 * @Description: java类作用描述
 * @Author: 毛小闯
 * @CreateDate: 2018/9/17 16:14
 */
@Configuration
@Slf4j
public class ShiroConfig {

    private static final String CACHE_KEY = "shiro:cache:";
    private static final String SESSION_KEY = "shiro:session:";
    private static final String NAME = "custom.name";
    private static final String VALUE = "/";
    @Autowired
    private ShiroService shiroService;


    @Bean("shiroFilterFactoryBean")
    public ShiroFilterFactoryBean shiroFilterFactoryBean(
            @Qualifier("securityManager")SecurityManager securityManager){
        //创建ShiroFilterFactoryBean对象
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        //配置shiro安全管理器SecurityMannager
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        //指定要求登录的连接
        shiroFilterFactoryBean.setLoginUrl("/user/unlogin");
        //指定登录成功后要跳转的连接
        shiroFilterFactoryBean.setSuccessUrl("/index");
        //未授权时跳转的界面
        shiroFilterFactoryBean.setUnauthorizedUrl("/user/unauth");
        //配置filterChainDefinitions拦截器
        Map<String, Filter> filterChainDefinitionMap = new LinkedHashMap<>(1);
        // 配置退出过滤器,具体的退出代码Shiro已经实现
//        filterChainDefinitionMap.put("/logout", "logout");
        //配置记住我或认证通过可以访问的地址
//        filterChainDefinitionMap.put("/user/userList", "user");
//        filterChainDefinitionMap.put("/", "user");
//
//		// 配置不会被拦截的链接 从上向下顺序判断
//        filterChainDefinitionMap.put("/login", "anon");
//        filterChainDefinitionMap.put("/css/*", "anon");
//        filterChainDefinitionMap.put("/js/*", "anon");
//        filterChainDefinitionMap.put("/js/*/*", "anon");
//        filterChainDefinitionMap.put("/js/*/*/*", "anon");
//        filterChainDefinitionMap.put("/images/*/**", "anon");
//        filterChainDefinitionMap.put("/layui/*", "anon");
//        filterChainDefinitionMap.put("/layui/*/**", "anon");
//        filterChainDefinitionMap.put("/treegrid/*", "anon");
//        filterChainDefinitionMap.put("/treegrid/*/*", "anon");
//        filterChainDefinitionMap.put("/fragments/*", "anon");
//        filterChainDefinitionMap.put("/layout", "anon");
//
//        filterChainDefinitionMap.put("/user/sendMsg", "anon");
//        filterChainDefinitionMap.put("/user/login", "anon");
//        filterChainDefinitionMap.put("/home", "anon");
//		/*filterChainDefinitionMap.put("/page", "anon");
//		filterChainDefinitionMap.put("/channel/record", "anon");*/
//
//		//add操作，该用户必须有【addOperation】权限
        //filterChainDefinitionMap.put("/user/setUser", "roles[superman]");
//        filterChainDefinitionMap.put("/user/delUser", "authc,perms[usermanage]");
//
//		// <!-- authc:所有url都必须认证通过才可以访问; anon:所有url都都可以匿名访问【放行】-->
//        filterChainDefinitionMap.put("/*", "authc");
//        filterChainDefinitionMap.put("/*/*", "authc");
//        filterChainDefinitionMap.put("/*/*/*", "authc");
//        filterChainDefinitionMap.put("/*/*/*/**", "authc");
        //设置自己的角色过滤器
        filterChainDefinitionMap.put("roles",rolesAuthorizationFilter());
        shiroFilterFactoryBean
                .setFilters(filterChainDefinitionMap);
        //设置初始化权限
        shiroFilterFactoryBean.setFilterChainDefinitionMap(shiroService.loadFilterChainDefinitions());
        log.debug("Shiro拦截器工厂类注入成功");
        return shiroFilterFactoryBean;

    }

    /**
     * @Description: 自定义角色过滤器
     * @author 毛小闯
     * @date 2018/9/21 10:45
     * @param
     * @return com.jjmf.permission_system.shiro.filter.CustomRolesAuthorizationFilter
     * @throws
     */
    @Bean
    public CustomRolesAuthorizationFilter rolesAuthorizationFilter() {
        return new CustomRolesAuthorizationFilter();
    }

    /**
     * @Description: shiro安全管理器设置realm认证
     * @author 毛小闯
     * @date 2018/9/17 16:34
     * @param
     * @return org.apache.shiro.mgt.SecurityManager
     * @throws
     */
    @Bean("securityManager")
    public SecurityManager securityManager(@Qualifier("authRealm") AuthRealm authRealm,
                                           @Qualifier("redisCacheManager") RedisCacheManager redisCacheManager,
                                           SessionManager sessionManager){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        //设置realm
        securityManager.setRealm(authRealm);
        // 设置缓存ehcache/redsi缓存管理器
        securityManager.setCacheManager(redisCacheManager);
        //设置session管理器
        securityManager.setSessionManager(sessionManager);
        return securityManager;
    }

    /**
     * @Description: 身份认证realm; (账号密码校验；权限等)
     * @author 毛小闯
     * @date 2018/9/17 16:21
     * @param
     * @throws
     */
    @Bean("authRealm")
    public AuthRealm authRealm(
            @Qualifier("redisCacheManager")RedisCacheManager redisCacheManager){
        AuthRealm realm = new AuthRealm();
        //设置缓存管理
        realm.setCacheManager(redisCacheManager);
        //如果配置了CacheManager，则设置是否应该使用认证缓存，否则就会被使用。
        //默认值是false，以保持向后兼容Shiro 1.1和更早的兼容性。
        //警告：如果安全缓存条件适用，只将该属性设置为true，
        //如在类级别JavaDoc中所记录的那样。
        realm.setAuthenticationCachingEnabled(false);
        //设置是否在配置了CacheManager时是否应该使用授权缓存，否则就会使用false。
        //默认是true
        realm.setAuthorizationCachingEnabled(false);
        return realm;

    }

    /**
     * @Description: 使用redis做缓存
     * @author 毛小闯
     * @date 2018/9/20 14:47
     * @param
     * @return org.springframework.data.redis.cache.RedisCacheManager
     * @throws
     */
    @Bean("redisCacheManager")
    public RedisCacheManager redisCacheManager(
            @Qualifier("redisManager") RedisManager redisManager){
        RedisCacheManager redisCacheManager = new RedisCacheManager();
        //把redismanages设置到redisCacheManager(使用redis做缓存)
        redisCacheManager.setRedisManager(redisManager);
        //设置过期时间
        redisCacheManager.setExpire(86400);
        //设置缓存的key
        redisCacheManager.setKeyPrefix(CACHE_KEY);
        return redisCacheManager;
    }

    /**
     * @Description: web应用Session管理器
     * (添加、删除SessionId到Cookie、读取Cookie获得SessionId)
     * @author 毛小闯
     * @date 2018/9/20 16:54
     * @param sessionDAO
     * @param simpleCookie
     * @return org.apache.shiro.web.session.mgt.DefaultWebSessionManager
     * @throws
     */
    @Bean
    public DefaultWebSessionManager defaultWebSessionManager(@Qualifier("redisSessionDAO") RedisSessionDAO sessionDAO,
                                                             @Qualifier("simpleCookie") SimpleCookie simpleCookie){
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        //设置sessionDao
        sessionManager.setSessionDAO(sessionDAO);
        //启用sessionIDCookie
        sessionManager.setSessionIdCookieEnabled(true);
        //设置simplecookie
        sessionManager.setSessionIdCookie(simpleCookie);
        return sessionManager;

    }
    
    /**
     * @Description: session
     * @author 毛小闯
     * @date 2018/9/20 16:56
     * @param redisManager
     * @return org.crazycake.shiro.RedisSessionDAO
     * @throws 
     */
    @Bean("redisSessionDAO")
    public RedisSessionDAO redisSessionDAO(
            @Qualifier("redisManager")RedisManager redisManager){
        RedisSessionDAO redisSessionDAO = new RedisSessionDAO();
        //把redismanages设置到redisSessionDAO(使用redis做session管理)
        redisSessionDAO.setRedisManager(redisManager);
        //设置过期时间
        redisSessionDAO.setExpire(864000);
        //设置缓存的key
        redisSessionDAO.setKeyPrefix(SESSION_KEY);
        return redisSessionDAO;
    }
    /**
     * @Description: 会话cookie(Cookie是SimpleCookie父类)
     * @author 毛小闯
     * @date 2018/9/20 16:47
     * @param
     * @return org.apache.shiro.web.servlet.SimpleCookie
     * @throws
     */
    @Bean("simpleCookie")
    public SimpleCookie simpleCookie(){
        SimpleCookie simpleCookie = new SimpleCookie();
        //设置cookie名字
        simpleCookie.setName(NAME);
        //设置Value
        simpleCookie.setValue(VALUE);
        return simpleCookie;

    }


    /**
     * @Description: Redis集群使用RedisClusterManager，单个Redis使用RedisManager
     * @author 毛小闯
     * @date 2018/9/20 14:43
     * @param redisProperties
     * @return org.crazycake.shiro.RedisManager
     * @throws 
     */
    @Bean("redisManager")
    public RedisManager redisManager(RedisProperties redisProperties){
        //使用RedisProperties读取配置文件中redis的信息并配置到RedisManager中
        //注意配置文件中redis的配置要以"spring.redis"开头
        RedisManager redisManager = new RedisManager();
        redisManager.setHost(redisProperties.getHost());
//        redisManager.setPassword(redisProperties.getPassword());
        redisManager.setPort(redisProperties.getPort());
        return redisManager;
    }
}
