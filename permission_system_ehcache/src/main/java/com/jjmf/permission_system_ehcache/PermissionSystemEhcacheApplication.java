package com.jjmf.permission_system_ehcache;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages ="com.jjmf.permission_system_ehcache.mapper" )
public class PermissionSystemEhcacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(PermissionSystemEhcacheApplication.class, args);
    }
}
