package com.jjmf.permission_system_ehcache.mapper;


import com.jjmf.permission_system_ehcache.pojo.Role;

import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    List<Role> getRoleList();

    Role getRoleByUserId(Integer userId);
}