package com.jjmf.permission_system_ehcache.mapper;


import com.jjmf.permission_system_ehcache.pojo.Authority;

import java.util.List;

public interface AuthorityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Authority record);

    int insertSelective(Authority record);

    Authority selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Authority record);

    int updateByPrimaryKey(Authority record);

    List<Authority> findAuthoritiesByRoleId(Integer roleId);

    List<Authority> findAuthorities();

}