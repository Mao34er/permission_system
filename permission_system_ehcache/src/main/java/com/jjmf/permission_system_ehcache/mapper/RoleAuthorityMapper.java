package com.jjmf.permission_system_ehcache.mapper;


import com.jjmf.permission_system_ehcache.pojo.RoleAuthorityKey;

public interface RoleAuthorityMapper {
    int deleteByPrimaryKey(RoleAuthorityKey key);

    int insert(RoleAuthorityKey record);

    int insertSelective(RoleAuthorityKey record);
}