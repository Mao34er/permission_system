package com.jjmf.permission_system_ehcache.service;



import com.jjmf.permission_system_ehcache.pojo.Role;

import java.util.List;

/**
 * <p>Company:河南巨匠魔方科技有限公司</p>
 *
 * @Description: java类作用描述
 * @Author: 毛小闯
 * @CreateDate: 2018/9/18 15:58
 */
public interface RoleService {

    /**
     * 获取角色列表
     * @return
     */
    List<Role> getRoleList();
}
