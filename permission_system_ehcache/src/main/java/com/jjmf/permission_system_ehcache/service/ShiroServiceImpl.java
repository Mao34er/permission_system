package com.jjmf.permission_system_ehcache.service;

import com.jjmf.permission_system_ehcache.mapper.AuthorityMapper;
import com.jjmf.permission_system_ehcache.pojo.Authority;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.mgt.DefaultFilterChainManager;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>Company:河南巨匠魔方科技有限公司</p>
 *
 * @Description: shiroservice
 * @Author: 毛小闯
 * @CreateDate: 2018/9/21 9:42
 */
@Service("shiroservice")
@Slf4j
public class ShiroServiceImpl implements ShiroService{

    @Autowired
    private AuthorityMapper authorityMapper;

    /**
     * @Description: 初始化权限
     * @author 毛小闯
     * @date 2018/9/21 9:48
     * @param 
     * @return java.util.Map<java.lang.String,java.lang.String>
     * @throws 
     */
    @Override
    public Map<String, String> loadFilterChainDefinitions() {
        //获取所有权限
        List<Authority> authorities = authorityMapper.findAuthorities();
        //权限控制map,从数据库获取
        Map<String,String> filterChainDefinitionMap = new LinkedHashMap<>();
        if (authorities.size()>0){
            String uris;
            String[] uriArr;
            for (Authority authority:authorities
                 ) {
                if (StringUtils.isEmpty(authority.getPermission())){
                    continue;
                }
                //这里我们使用单uri
                uris=authority.getUri();
                //这一步可以省略
                uriArr=uris.split(",");
                if (uriArr.length==1){
                    filterChainDefinitionMap.put(uris,authority.getPermission());
                }
                for (String uri:uriArr
                     ) {
                    filterChainDefinitionMap.put(uri,authority.getPermission());
                }
            }
        }
        filterChainDefinitionMap.put("/user/ajaxLogin", "anon");
        //配置退出 过滤器,其中的具体的退出代码Shiro已经替我们实现了
        filterChainDefinitionMap.put("/user/logout", "anon");
        filterChainDefinitionMap.put("/role/list", "anon");
//        filterChainDefinitionMap.put("/**", "authc");
        filterChainDefinitionMap.put("/**", "authc");
        log.debug("initialize shiro permission success...");
        return filterChainDefinitionMap;
    }
    
    /**
     * @Description: 在对角色进行增删改操作时，需要调用此方法进行动态刷新
     * @author 毛小闯
     * @date 2018/9/21 9:49
     * @return void
     * @throws 
     */
    @Override
    public void updatePermission(ShiroFilterFactoryBean shiroFilterFactoryBean) {
        //多线程同步代码块
        synchronized (this){
            AbstractShiroFilter shiroFilter;
            try{
                shiroFilter= (AbstractShiroFilter) shiroFilterFactoryBean.getObject();
            }catch (Exception e){
                throw new RuntimeException("get ShiroFilter from shiroFilterFactoryBean error!");
            }
            //获取过滤管理器
            PathMatchingFilterChainResolver filterChainResolver = (PathMatchingFilterChainResolver) shiroFilter.getFilterChainResolver();
            DefaultFilterChainManager manager = (DefaultFilterChainManager) filterChainResolver.getFilterChainManager();

            //清空老的权限控制
            manager.getFilterChains().clear();
            //清空老的权限map
            shiroFilterFactoryBean.getFilterChainDefinitionMap().clear();
            //设置新的权限map
            shiroFilterFactoryBean.setFilterChainDefinitionMap(loadFilterChainDefinitions());
            //重新构建生成
            Map<String, String> chains = shiroFilterFactoryBean.getFilterChainDefinitionMap();
            for (Map.Entry<String,String> entry:chains.entrySet()
                 ) {
                String url = entry.getKey();
                String chainDefinition = entry.getValue().trim().replace(" ","");
                manager.createChain(url,chainDefinition);
            }
            log.debug("update shiro permission success...");
        }
    }
}
