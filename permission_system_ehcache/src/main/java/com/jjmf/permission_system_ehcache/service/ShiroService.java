package com.jjmf.permission_system_ehcache.service;


import org.apache.shiro.spring.web.ShiroFilterFactoryBean;

import java.util.Map;

/**
 * <p>Company:河南巨匠魔方科技有限公司</p>
 *
 * @Description: java类作用描述
 * @Author: 毛小闯
 * @CreateDate: 2018/9/21 9:45
 */
public interface ShiroService {
    /**
     * @Description: 初始化权限
     * @author 毛小闯
     * @date 2018/9/21 9:46
     * @param
     * @return java.util.Map<java.lang.String,java.lang.String>
     * @throws
     */
    Map<String,String> loadFilterChainDefinitions();
    
    /**
     * @Description: 在对角色进行增删改操作时，需要调用此方法进行动态刷新
     * @author 毛小闯
     * @date 2018/9/21 9:47
     * @return void
     * @throws 
     */
    void updatePermission(ShiroFilterFactoryBean shiroFilterFactoryBean);



}
