package com.jjmf.permission_system_ehcache.pojo;

public class RoleAuthorityKey {
    private Integer roleId;

    private Integer authorityId;

    public RoleAuthorityKey(Integer roleId, Integer authorityId) {
        this.roleId = roleId;
        this.authorityId = authorityId;
    }

    public RoleAuthorityKey() {
        super();
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(Integer authorityId) {
        this.authorityId = authorityId;
    }
}