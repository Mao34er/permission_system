package com.jjmf.permission_system_ehcache.config;

import com.jjmf.permission_system_ehcache.service.ShiroService;
import com.jjmf.permission_system_ehcache.shiro.AuthRealm;
import com.jjmf.permission_system_ehcache.shiro.filter.CustomRolesAuthorizationFilter;
import lombok.extern.slf4j.Slf4j;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.io.ResourceUtils;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>Company:河南巨匠魔方科技有限公司</p>
 *
 * @Description: java类作用描述
 * @Author: 毛小闯
 * @CreateDate: 2018/9/17 16:14
 */
@Configuration
@Slf4j
public class ShiroConfig {

    private static final String CACHE_KEY = "shiro:cache:";
    private static final String SESSION_KEY = "shiro:session:";
    private static final String NAME = "custom.name";
    private static final String VALUE = "/";

    @Autowired
    private ShiroService shiroService;


    /**
     * ShiroFilterFactoryBean 处理拦截资源文件过滤器
     *	</br>1,配置shiro安全管理器接口securityManage;
     *	</br>2,shiro 连接约束配置filterChainDefinitions;
     */
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(
            SecurityManager securityManager){
        //创建ShiroFilterFactoryBean对象
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        //配置shiro安全管理器SecurityMannager
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        //指定要求登录的连接
        shiroFilterFactoryBean.setLoginUrl("/user/unlogin");
        //指定登录成功后要跳转的连接
        shiroFilterFactoryBean.setSuccessUrl("/index");
        //未授权时跳转的界面
        shiroFilterFactoryBean.setUnauthorizedUrl("/user/unauth");
        //配置filterChainDefinitions拦截器
        Map<String, Filter> filterChainDefinitionMap = new LinkedHashMap<>(1);
        //设置自己的角色过滤器
        filterChainDefinitionMap.put("roles",rolesAuthorizationFilter());
        shiroFilterFactoryBean
                .setFilters(filterChainDefinitionMap);
        //设置初始化权限
        shiroFilterFactoryBean.setFilterChainDefinitionMap(shiroService.loadFilterChainDefinitions());
        log.debug("Shiro拦截器工厂类注入成功");
        return shiroFilterFactoryBean;

    }

    /**
     * @Description: 自定义角色过滤器
     * @author 毛小闯
     * @date 2018/9/21 10:45
     * @param
     * @return com.jjmf.permission_system.shiro.filter.CustomRolesAuthorizationFilter
     * @throws
     */
    @Bean
    public CustomRolesAuthorizationFilter rolesAuthorizationFilter() {
        return new CustomRolesAuthorizationFilter();
    }

    /**
     * @Description: shiro安全管理器设置realm认证
     * @author 毛小闯
     * @date 2018/9/17 16:34
     * @param
     * @return org.apache.shiro.mgt.SecurityManager
     * @throws
     */
    @Bean("securityManager")
    public SecurityManager securityManager(){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        //设置realm
        securityManager.setRealm(authRealm());
        // 设置缓存ehcache/redsi缓存管理器
        securityManager.setCacheManager(ehCacheManager());
        //设置session管理器
//        securityManager.setSessionManager(sessionManager);
        return securityManager;
    }

    /**
     * @Description: 身份认证realm; (账号密码校验；权限等)
     * @author 毛小闯
     * @date 2018/9/17 16:21
     * @param
     * @throws
     */
    @Bean("authRealm")
    public AuthRealm authRealm(){
        AuthRealm realm = new AuthRealm();
        //设置缓存管理
//        realm.setCacheManager(redisCacheManager);
        //如果配置了CacheManager，则设置是否应该使用认证缓存，否则就会被使用。
        //默认值是false，以保持向后兼容Shiro 1.1和更早的兼容性。
        //警告：如果安全缓存条件适用，只将该属性设置为true，
        //如在类级别JavaDoc中所记录的那样。
        realm.setAuthenticationCachingEnabled(false);
        //设置是否在配置了CacheManager时是否应该使用授权缓存，否则就会使用false。
        //默认是true
        realm.setAuthorizationCachingEnabled(false);
        return realm;

    }
    /**
     * @Description: 会话cookie(Cookie是SimpleCookie父类)
     * @author 毛小闯
     * @date 2018/9/20 16:47
     * @param
     * @return org.apache.shiro.web.servlet.SimpleCookie
     * @throws
     */
    @Bean("simpleCookie")
    public SimpleCookie simpleCookie(){
        SimpleCookie simpleCookie = new SimpleCookie();
        //设置cookie名字
        simpleCookie.setName(NAME);
        //设置Value
        simpleCookie.setValue(VALUE);
        return simpleCookie;

    }

    /**
     * ehcache缓存管理器；shiro整合ehcache：
     * 通过安全管理器：securityManager
     * 单例的cache防止热部署重启失败
     * @return EhCacheManager
     */
    @Bean("ehCacheManager")
    public EhCacheManager ehCacheManager(){
        log.debug("shiro使用ehcache做缓存");
        EhCacheManager ehcache = new EhCacheManager();
       CacheManager cacheManager = CacheManager.getCacheManager("es");
       if (cacheManager == null){
           try{
                cacheManager = CacheManager.create(ResourceUtils.getInputStreamForPath("classpath:config/ehcache.xml"));
           }catch (CacheException | IOException e){
               e.printStackTrace();
           }
       }
       ehcache.setCacheManager(cacheManager);
       return ehcache;

    }
}
