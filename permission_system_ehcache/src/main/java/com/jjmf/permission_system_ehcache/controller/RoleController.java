package com.jjmf.permission_system_ehcache.controller;

import com.jjmf.permission_system_ehcache.pojo.Role;
import com.jjmf.permission_system_ehcache.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>Company:河南巨匠魔方科技有限公司</p>
 *
 * @Description: java类作用描述
 * @Author: 毛小闯
 * @CreateDate: 2018/9/21 16:51
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @RequestMapping("/list")
    public List<Role> list(){
        return roleService.getRoleList();
    }
    @RequestMapping("/list/copy1")
    public List<Role> listcopy(){
        return roleService.getRoleList();
    }
}
