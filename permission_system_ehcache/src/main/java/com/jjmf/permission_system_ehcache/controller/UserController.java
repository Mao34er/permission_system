package com.jjmf.permission_system_ehcache.controller;

import com.alibaba.fastjson.JSONObject;
import com.jjmf.permission_system_ehcache.pojo.User;
import com.jjmf.permission_system_ehcache.service.RoleService;
import com.jjmf.permission_system_ehcache.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>Company:河南巨匠魔方科技有限公司</p>
 *
 * @Description: java类作用描述
 * @Author: 毛小闯
 * @CreateDate: 2018/9/21 16:16
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    @RequestMapping("/unlogin")
    public Map<String,Object> unlogin(){
        Map<String,Object> reslutMap = new HashMap<>();
        reslutMap.put("code" ,100);
        reslutMap.put("msg" ,"没有登录");
        return reslutMap;
    }
    /**
     * 登录方法
     * @param userInfo
     * @return
     */
    @RequestMapping(value = "/ajaxLogin", method = RequestMethod.POST)
    public String ajaxLogin(User userInfo) {
        JSONObject jsonObject = new JSONObject();
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(userInfo.getUsername(),DigestUtils.md5Hex(userInfo.getPassword()));
        try {
            subject.login(token);
            jsonObject.put("token", subject.getSession().getId());
            jsonObject.put("msg", "登录成功");
        } catch (IncorrectCredentialsException e) {
            e.printStackTrace();
            jsonObject.put("msg", "密码错误");
        } catch (LockedAccountException e) {
            e.getStackTrace();
            jsonObject.put("msg", "登录失败，该用户已被冻结");
        } catch (AuthenticationException e) {
            jsonObject.put("msg", "该用户不存在");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @RequestMapping("/unauth")
    public Map<String,Object> unauth(){
        Map<String,Object> reslutMap = new HashMap<>();
        reslutMap.put("code" ,300);
        reslutMap.put("msg" ,"没有权限");
        return reslutMap;
    }

    @RequestMapping("/list")
    public List<User> list(){
        return userService.getUserList();
    }

}
