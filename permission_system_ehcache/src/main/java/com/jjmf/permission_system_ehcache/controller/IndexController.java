package com.jjmf.permission_system_ehcache.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Company:河南巨匠魔方科技有限公司</p>
 *
 * @Description: java类作用描述
 * @Author: 毛小闯
 * @CreateDate: 2018/9/21 16:36
 */
@RestController
@RequestMapping("/test")
public class IndexController {

    @RequestMapping("/index")
    public String index(){
        return "欢迎来到主页!";
    }
}
